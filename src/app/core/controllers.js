(function () {

  "use strict";

  var controllers = angular.module("app.controllers", ["app.services"]);

  controllers.constant("carousel", oocss.carousel);

  controllers.controller("appController",
    function appController($scope, hashClickMonitorService, carousel) {

      function setupDecimalInputEvents() {

        // 1 decimal point spinner controls
        $(".decimalInput").spinner({
          step: 0.1,
          numberFormat: "n",
          stop: function () {
            $(this).change()
          }
        });

        // 3 decimal point spinner controls
        $(".decimal3Input").spinner({
          step: 0.001,
          numberFormat: "n",
          stop: function () {
            $(this).change()
          }
        });

        // whole number spinner controls
        $(".numberInput").spinner({
          step: 1,
          numberFormat: "n",
          stop: function () {
            $(this).change()
          }
        });
      }

      function setupNavEvents() {

        // carousel select
        $(".nav a").click(function () {
          // get the current jquery this
          var $this = $(this);

          // get the current active
          var $active = $(".nav .navLinkActive");

          // check it's not already selected
          if ($active.attr("href") === $this.attr("href")) {
            return
          }

          // get the link index
          var linkIndex = $(".nav a").index(this);

          // animate the carousel to the new link index
          carousel.select(".carousel", linkIndex);

          // clear prev active link
          $active.toggleClass("navLinkActive");

          // set this a link to be active
          $this.toggleClass("navLinkActive");

          this.scrollChanged = true;
        });

      }
      $scope.init = function () {

        $(document).ready(function () {
          setupDecimalInputEvents();
          setupNavEvents();

          hashClickMonitorService.setup();

        });

      }

    }
  );

  controllers.controller("bpmController",
    function bpmController($scope, musicMathService) {

      $scope.noteTypes = musicMathService.noteTypes;

      // convert bpm
      $scope.convertBpmValue = 120;
      $scope.onConvertBpmChange = function () {

        var Bpm = parseFloat($scope.convertBpmValue);
        var hz = musicMathService.bpmToHz(Bpm);
        var khz = hz / 1000;
        var sec = 1 / hz;
        var ms = sec * 1000;
        var bpmNote = musicMathService.bpmToMidiNote(Bpm);

        $scope.convertBpmResults = {
          hz: hz.toFixed(musicMathService.decimalPlaces),
          khz: khz.toFixed(musicMathService.decimalPlaces),
          ms: ms.toFixed(musicMathService.decimalPlaces),
          sec: sec.toFixed(musicMathService.decimalPlaces),
          note: bpmNote.noteName,
          octave: bpmNote.octave,
          cents: bpmNote.cents.toFixed(musicMathService.decimalPlaces)
        };

        $scope.convertBpmResultsColumns = Object.keys($scope.convertBpmResults);

      };

      // bpm delay timing
      $scope.bpmDelayTimingA = 120;
      $scope.bpmDelayTimingB = 4;
      $scope.bpmDelayTimingC = "Normal";
      $scope.onBpmDelayTimingChange = function () {
        var bpm = $scope.bpmDelayTimingA;
        var numberOfBars = 1;
        var beatsPerBar = $scope.bpmDelayTimingB;
        var noteType = $scope.bpmDelayTimingC;

        var beatsPerSec = parseFloat(60 / bpm);
        var barsLength = parseFloat(beatsPerSec * 4);
        var result = parseFloat(barsLength / (beatsPerBar * numberOfBars));

        switch (noteType) {
          case "Dotted":
            result += (result / 2);
            break;
          case "Triplets":
            result -= (result / 3);
            break;
        }

        var ms = result * 1000;
        var hz = (1 / ms) * 1000;
        var DelayBpm = musicMathService.hzToBpm(hz);
        var midiNoteInfo = musicMathService.bpmToMidiNote(DelayBpm);

        $scope.bpmDelayTimingResults = {
          hz: hz.toFixed(musicMathService.decimalPlaces),
          khz: (hz / 1000).toFixed(musicMathService.decimalPlaces),
          ms: ms.toFixed(musicMathService.decimalPlaces),
          sec: result.toFixed(musicMathService.decimalPlaces),
          note: midiNoteInfo.noteName,
          octave: midiNoteInfo.octave,
          cents: midiNoteInfo.cents.toFixed(musicMathService.decimalPlaces)
        };

        $scope.bpmDelayTimingResultsColumns = Object.keys($scope.bpmDelayTimingResults);
      };

      // bpm time stretch
      $scope.bpmTimeStretchA = 120;
      $scope.bpmTimeStretchB = 120;
      $scope.onBpmTimeStretchChange = function () {

        var TimeStretchBpmA = parseFloat($scope.bpmTimeStretchA);
        var TimeStretchBpmB = parseFloat($scope.bpmTimeStretchB);
        var divisor = (TimeStretchBpmB / TimeStretchBpmA);

        var ratio = 100 / divisor;
        var percentDiff = 100 / (TimeStretchBpmA / (TimeStretchBpmB - TimeStretchBpmA));
        //var semitones = musicMathService.semitoneDiff(TimeStretchBpmA, TimeStretchBpmB);

        $scope.bpmTimeStretchResults = {
          ratio: ratio.toFixed(musicMathService.decimalPlaces),
          '% change': percentDiff.toFixed(musicMathService.decimalPlaces)
        };

        $scope.bpmTimeStretchResultsColumns = Object.keys($scope.bpmTimeStretchResults);
      };

      // bpm transpose by percentage
      $scope.transposeBpmByPercentageA = 120;
      $scope.transposeBpmByPercentageB = 1;
      $scope.onBpmTransposeByPercentageChange = function () {

        var Bpm = parseFloat($scope.transposeBpmByPercentageA);
        var Percentage = parseFloat($scope.transposeBpmByPercentageB);

        var bpmChange = Bpm * (Percentage / 100);
        var newBpm = Bpm + bpmChange;
        var semitones = musicMathService.semitoneDiff(Bpm, newBpm);
        var midiNoteInfo = musicMathService.bpmToMidiNote(newBpm);

        $scope.bpmTransposeByPercentageResults = {
          bpm: newBpm.toFixed(musicMathService.decimalPlaces),
          semitones: semitones.toFixed(musicMathService.decimalPlaces),
          note: midiNoteInfo.noteName,
          octave: midiNoteInfo.octave,
          cents: midiNoteInfo.cents.toFixed(musicMathService.decimalPlaces)
        };

        $scope.bpmTransposeByPercentageResultsColumns = Object.keys($scope.bpmTransposeByPercentageResults);
      };

      // bpm transpose by semitones
      $scope.transposeBpmBySemitonesA = 120;
      $scope.transposeBpmBySemitonesB = 1;
      $scope.onBpmTransposeBySemitonesChange = function () {

        var bpm = $scope.transposeBpmBySemitonesA;
        var Semitones = $scope.transposeBpmBySemitonesB;

        var newBpm = musicMathService.semitoneShift(bpm, Semitones);
        var percentDiff = 100 / (bpm / (newBpm - bpm));
        var midiNoteInfo = musicMathService.bpmToMidiNote(newBpm);

        $scope.bpmTransposeBySemitonesResults = {
          bpm: newBpm.toFixed(musicMathService.decimalPlaces),
          '% change': percentDiff.toFixed(musicMathService.decimalPlaces),
          note: midiNoteInfo.noteName,
          octave: midiNoteInfo.octave,
          cents: midiNoteInfo.cents.toFixed(musicMathService.decimalPlaces)
        };

        $scope.bpmTransposeBySemitonesResultsColumns = Object.keys($scope.bpmTransposeBySemitonesResults);

      };

      // generate bpm table
      $scope.generateBpmChartA = 60;
      $scope.generateBpmChartB = 200;
      $scope.generateBpmChartC = 20;
      $scope.onGenerateNoteBpmTableChange = function () {

        var bpmA = parseFloat($scope.generateBpmChartA) || 60.0;
        var bpmB = parseFloat($scope.generateBpmChartB) || 200.0;
        var stepBpm = parseFloat($scope.generateBpmChartC) || 10.0;

        var showBpm = true;
        var showHz = false;
        var showKhz = false;
        var showMs = false;
        var showSec = false;
        var showNote = true;
        var showCents = true;
        var showOctave = true;

        var hz;
        var khz;
        var sec;
        var ms;
        var bpmNote;

        //				if (showBpm) headers.push("<strong>bpm</strong>");
        //				if (showHz) headers.push("<strong>hz</strong>");
        //				if (showKhz) headers.push("<strong>khz</strong>");
        //				if (showMs) headers.push("<strong>ms</strong>");
        //				if (showSec) headers.push("<strong>sec</strong>");
        //				if (showNote) headers.push("<strong>note</strong>");
        //				if (showCents) headers.push("<strong>cents</strong>");
        //				if (showOctave) headers.push("<strong>octave</strong>");

        $scope.generateNoteBpmTableResults = {
          Bpm: [],
          Note: [],
          Cents: [],
          Octave: []
        };

        for (var bpm = bpmA; bpm <= bpmB; bpm += stepBpm) {

          hz = musicMathService.bpmToHz(bpm);
          khz = hz / 1000;
          sec = 1 / hz;
          ms = sec * 1000;
          bpmNote = musicMathService.bpmToMidiNote(bpm);

          //rows = [];
          if (showBpm) $scope.generateNoteBpmTableResults.Bpm.push(bpm.toFixed(musicMathService.decimalPlaces));
          if (showHz) $scope.generateNoteBpmTableResults.Hz.push(hz.toFixed(musicMathService.decimalPlaces));
          if (showKhz) $scope.generateNoteBpmTableResults.Khz.push(khz.toFixed(musicMathService.decimalPlaces));
          if (showMs) $scope.generateNoteBpmTableResults.Ms.push(ms.toFixed(musicMathService.decimalPlaces));
          if (showSec) $scope.generateNoteBpmTableResults.Sec.push(sec.toFixed(musicMathService.decimalPlaces));
          if (showNote) $scope.generateNoteBpmTableResults.Note.push(bpmNote.noteName);
          if (showCents) $scope.generateNoteBpmTableResults.Cents.push(bpmNote.cents.toFixed(musicMathService.decimalPlaces));
          if (showOctave) $scope.generateNoteBpmTableResults.Octave.push(bpmNote.octave);

        }

        $scope.generateNoteBpmTableResultsColumns = Object.keys($scope.generateNoteBpmTableResults);

      };

      // controller init method
      $scope.init = function () {
        $scope.onConvertBpmChange();
        $scope.onBpmDelayTimingChange();
        $scope.onBpmTimeStretchChange();
        $scope.onBpmTransposeByPercentageChange();
        $scope.onBpmTransposeBySemitonesChange();
        $scope.onGenerateNoteBpmTableChange()
      }

    }
  );

  controllers.controller("hertzController",
    function hertzController($scope, musicMathService) {

      // convert hertz
      $scope.convertHertzA = 440;

      $scope.onConvertHertzChange = function () {

        var hz = parseFloat($scope.convertHertzA);
        var bpm = musicMathService.hzToBpm(hz);
        var sec = 1 / hz;
        var ms = sec * 1000;
        var midiNoteInfo = musicMathService.bpmToMidiNote(bpm);

        $scope.convertHertzResults = {
          //bpm: bpm.toFixed(musicMathService.decimalPlaces),
          khz: (hz / 1000).toFixed(musicMathService.decimalPlaces),
          ms: ms.toFixed(musicMathService.decimalPlaces),
          sec: sec.toFixed(musicMathService.decimalPlaces),
          note: midiNoteInfo.noteName,
          octave: midiNoteInfo.octave,
          cents: midiNoteInfo.cents.toFixed(musicMathService.decimalPlaces)
        };

        $scope.convertHertzResultsColumns = Object.keys($scope.convertHertzResults);

      };

      // controller init method
      $scope.init = function () {
        $scope.onConvertHertzChange();
      }

    }
  );

  controllers.controller("noteController",
    function noteController($scope, musicMathService) {

      $scope.noteNames = musicMathService.noteNames;
      $scope.octaves = musicMathService.octaves;

      // convert hertz
      $scope.convertNoteA = 0; // C
      $scope.convertNoteB = 4; // 4
      $scope.onConvertNoteChange = function () {

        var noteAValue = $scope.convertNoteA;
        var octaveAValue = $scope.convertNoteB;

        var hz = musicMathService.noteToHz(noteAValue - 9, octaveAValue - 4);
        var ms = (1 / hz) * 1000;
        var sec = ms / 1000;
        //var bpm = hz * 60;

        $scope.convertNoteResults = {
          hz: hz.toFixed(musicMathService.decimalPlaces),
          khz: (hz / 1000).toFixed(musicMathService.decimalPlaces),
          ms: ms.toFixed(musicMathService.decimalPlaces),
          sec: sec.toFixed(musicMathService.decimalPlaces)
          //bpm: bpm.toFixed(musicMathService.decimalPlaces)
        };

        $scope.convertNoteResultsColumns = Object.keys($scope.convertNoteResults);

      };

      // note diff in hertz
      $scope.noteDiffInHertzA = 0;
      $scope.noteDiffInHertzB = 4;
      $scope.noteDiffInHertzC = 0;
      $scope.noteDiffInHertzD = 5;
      $scope.onNoteDiffInHertzChange = function () {

        var noteAValue = $scope.noteDiffInHertzA;
        var octaveAValue = $scope.noteDiffInHertzB;

        var noteBValue = $scope.noteDiffInHertzC;
        var octaveBValue = $scope.noteDiffInHertzD;

        var hz = musicMathService.noteToHz(noteBValue - 9, octaveBValue - 4) - musicMathService.noteToHz(noteAValue - 9, octaveAValue - 4);
        var ms = (1 / hz) * 1000;
        var sec = ms / 1000;
        var bpm = hz * 60;

        $scope.noteDiffInHertzResults = {
          hz: hz.toFixed(musicMathService.decimalPlaces),
          khz: (hz / 1000).toFixed(musicMathService.decimalPlaces),
          ms: ms.toFixed(musicMathService.decimalPlaces),
          sec: sec.toFixed(musicMathService.decimalPlaces),
          bpm: bpm.toFixed(musicMathService.decimalPlaces)
        };

        $scope.noteDiffInHertzResultsColumns = Object.keys($scope.noteDiffInHertzResults);

      };

      // semitone diff
      $scope.semitoneDifferenceA = 0;
      $scope.semitoneDifferenceB = 4;
      $scope.semitoneDifferenceC = 1;
      $scope.onSemitoneDifferenceChange = function () {
        var noteAValue = $scope.semitoneDifferenceA;
        var octaveAValue = $scope.semitoneDifferenceB;
        var semitonesToShift = $scope.semitoneDifferenceC;

        var hz = musicMathService.noteToHz(noteAValue - 9, octaveAValue - 4);
        var hzDiff = musicMathService.semitoneShift(hz, semitonesToShift) - hz;

        var ms = (1 / hzDiff) * 1000;
        var sec = ms / 1000;
        var bpm = hzDiff * 60;

        $scope.semitoneDifferenceResults = {
          hz: hzDiff.toFixed(musicMathService.decimalPlaces),
          khz: (hzDiff / 1000).toFixed(musicMathService.decimalPlaces),
          ms: ms.toFixed(musicMathService.decimalPlaces),
          sec: sec.toFixed(musicMathService.decimalPlaces),
          bpm: bpm.toFixed(musicMathService.decimalPlaces)
        };

        $scope.semitoneDifferenceResultsColumns = Object.keys($scope.semitoneDifferenceResults);

      };

      // generate note hertz table
      $scope.generateNoteHertzTableA = 0;
      $scope.onGenerateNoteHertzTableChange = function () {
        var noteNumber = $scope.generateNoteHertzTableA;
        var noteName = musicMathService.noteNames[noteNumber];

        $scope.generateNoteHertzTableResults = {
          Note: [],
          Hz: []
        };

        for (var octave = 0; octave < 11; octave++) {
          $scope.generateNoteHertzTableResults.Note.push(noteName + octave);
          $scope.generateNoteHertzTableResults.Hz.push(musicMathService.noteToHz(noteNumber - 9, octave - 4).toFixed(musicMathService.decimalPlaces));
        }

        $scope.generateNoteHertzTableResultsColumns = Object.keys($scope.generateNoteHertzTableResults);
      };

      // generate note octave table
      $scope.generateNoteOctaveHertzTableA = 4;
      $scope.onGenerateNoteOctaveHertzTable = function () {
        var octaveNumber = $scope.generateNoteOctaveHertzTableA;

        $scope.generateNoteOctaveHertzTableResults = {
          Note: [],
          Hz: []
        };

        for (var note = 0; note < 12; note++) {
          $scope.generateNoteOctaveHertzTableResults.Note.push(musicMathService.noteNames[note] + octaveNumber);
          $scope.generateNoteOctaveHertzTableResults.Hz.push(musicMathService.noteToHz(note - 9, octaveNumber - 4).toFixed(musicMathService.decimalPlaces));
        }

        $scope.generateNoteOctaveHertzTableResultsColumns = Object.keys($scope.generateNoteOctaveHertzTableResults);

      };

      // controller init method
      $scope.init = function () {
        $scope.onConvertNoteChange();
        $scope.onNoteDiffInHertzChange();
        $scope.onSemitoneDifferenceChange();
        $scope.onGenerateNoteHertzTableChange();
        $scope.onGenerateNoteOctaveHertzTable();
      };

    }
  );


  controllers.controller("qualityFactorController",
    function qualityFactorController($scope, musicMathService) {

      $scope.octaves = musicMathService.octaves;
      $scope.semitones = musicMathService.semitones;

      // convert bw to Q
      $scope.convertBWtoQA = 1;
      $scope.convertBWtoQB = 0;
      $scope.onConvertBWtoQChange = function () {

        var octaveA = $scope.convertBWtoQA;
        var SemitoneA = $scope.convertBWtoQB;

        var octaveAValue = parseFloat(octaveA);
        var SemitoneAValue = parseFloat(SemitoneA);

        $scope.convertBWtoQResults = {
          Q: musicMathService.octaveBWtoQ(octaveAValue + (SemitoneAValue * musicMathService.semitoneQBWConst)).toFixed(musicMathService.decimalPlaces),
          bandwidth: (octaveAValue + (SemitoneAValue * musicMathService.semitoneQBWConst)).toFixed(musicMathService.decimalPlaces)
        };

        $scope.convertBWtoQResultsColumns = Object.keys($scope.convertBWtoQResults);

      };

      // convert q to bw
      $scope.convertQtoBWA = 1.414;
      $scope.onConvertQtoBWChange = function () {

        var qualityFactor = parseFloat($scope.convertQtoBWA);

        if (qualityFactor < 0) {
          $scope.convertQtoBWA = 0;
        }
        else if (qualityFactor <= 34.623) {

          var bandWidth = musicMathService.qToOctaveBW(qualityFactor);
          var octaves = bandWidth / musicMathService.octaveQBWConst;
          var semitones = (bandWidth / musicMathService.semitoneQBWConst) % 12;
          var cents = (semitones - Math.round(semitones)) * 100;

          $scope.convertQtoBWResults = {
            octaves: parseInt(octaves),
            semitones: Math.round(semitones),
            cents: cents.toFixed(musicMathService.decimalPlaces),
            bandWidth: bandWidth.toFixed(musicMathService.decimalPlaces)
          };

          $scope.convertQtoBWResultsColumns = Object.keys($scope.convertQtoBWResults);


        } else {
          $scope.convertQtoBWA = "34.623";
        }

      };

      // controller init method
      $scope.init = function () {
        $scope.onConvertBWtoQChange();
        $scope.onConvertQtoBWChange();
      };

    }
  );


}());