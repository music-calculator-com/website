(function () {

  "use strict";

  var services = angular.module("app.services", ["ngResource"]);

  services.service("musicMathService", function () {

    var log2 = Math.log(2);
    //	var decimalPlaces = 3;

    var service = {
      // constants
      decimalPlaces: 3,
      noteNames: [
        "C",
        "C#/Db",
        "D",
        "D#/Eb",
        "E",
        "F",
        "F#/Gb",
        "G",
        "G#/Ab",
        "A",
        "A#/Bb",
        "B"
      ],
      noteTypes: [
        "Normal",
        "Dotted",
        "Triplets"
      ],
      octaves: [
        0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10
      ],
      semitones: [
        0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11
      ],
      // ================
      // Hz methods
      // ================
      noteToHz: function (note, octave) {
        return service.semitoneShift(440, parseFloat(note) + parseFloat(octave * 12));
      },
      hzToBpm: function (hzValue) {
        return hzValue * 60;
      },
      // ================
      // Quality factor methods
      // ================
      octaveQBWConst: 0.999999999999999999999999999996,
      semitoneQBWConst: 0.083333333333333333333333333333333,
      octaveBWtoQ: function (octaveValue) {
        return Math.sqrt(Math.pow(2, octaveValue)) / (Math.pow(2, octaveValue) - 1);
      },
      qToOctaveBW: function (qualityFactor) {
        var matrix1 = ((qualityFactor * qualityFactor * 2) + 1) / (qualityFactor * qualityFactor * 2);
        var matrix2 = Math.pow(matrix1, 2);
        var matrix3 = Math.sqrt(matrix2 - 1);
        var matrix4 = matrix1 + matrix3;
        return Math.log(matrix4) / log2;
      },
      // ================
      // BPM methods
      // ================
      bpmToHz: function (bpmValue) {
        return bpmValue / 60;
      },
      bpmToMidiNote: function (bpm) {
        var hz = service.bpmToHz(bpm);
        var powerDiff = Math.log(hz / 440) / log2;
        var noteValue = 189 + (12 * powerDiff);
        var noteIndex = Math.round(noteValue);

        return {
          octave: Math.round(powerDiff) + 4,
          noteName: service.noteNames[Math.abs(noteIndex) % 12],
          cents: (noteValue - noteIndex) * 100
        };
      },
      // ================
      // Semitone methods
      // ================
      semitoneShift: function (value, amount) {
        return value / Math.pow(2, amount / -12);
      },
      semitoneDiff: function (ValueA, ValueB) {
        return (12 / Math.log(2)) * Math.log(ValueB / ValueA);
      }
    };

    return service;
  });


  services.service("hashClickMonitorService", function () {

    function hashChanged(defaultHashAnchor, track) {
      var anchorHash = (window.location.hash === "") ? window.location.search.replace("?", "#") : window.location.hash;
      anchorHash = anchorHash.replace("_", "-").toLowerCase();
      if (anchorHash.length > 0)
        $(".nav a[href='" + anchorHash + "']").click();
      else
        $(".nav a[href=defaultHashAnchor]").click();

      if (track === true) {
        gtag('event', 'click', {
          event_category: 'anchor',
          event_label: location.hash,
          value: 1
        });
      }
    }

    return {

      setup: function (defaultHashAnchor) {

        // get the anchor (if any) and execute it's click
        hashChanged(defaultHashAnchor, false);

        if ("onhashchange" in window) { // event supported?

          window.onhashchange = function () {
            hashChanged(defaultHashAnchor, true);
          };

        } else { // event not supported:

          var storedHash = window.location.hash;

          this.hashMonitorIntervalId = window.setInterval(function () {
            if (window.location.hash !== storedHash) {
              storedHash = window.location.hash;
              hashChanged(defaultHashAnchor, true);
            }
          }, 100);

        }
      }

    };

  });

}());

