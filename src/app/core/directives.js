(function () {

  "use strict";
  var directives = angular.module("app.directives", []);

	/**
	 * Example:
	 *
	 *  <input ng-model="myModel" ui-spinner ui-spinner-change="myChangeHandler()" />
	 *
	 */
  directives.directive('uiSpinner',
    function ($timeout) {

      return {
        restrict: 'A',
        require: 'ngModel',
        scope: {
          uiSpinnerChange: '&'
        },
        link: function (scope, element, attr, ngModel) {

          var onSpin = function (event, ui) {
            // update and apply the new view value
            ngModel.$setViewValue(ui.value);
            scope.$apply();

            // pass on to the uiSpinnerChange handler
            $timeout(function () {
              scope.uiSpinnerChange(event);
            });
          };

          // listen for the spin event
          $(element).on("spin", onSpin);

          // listen for input changes and pass on to uiSpinnerChange handler
          element[0].addEventListener("keyup", function (event) {
            $timeout(function () {
              scope.uiSpinnerChange(event);
            });
          });

        }

      };

    }
  );

  directives.directive('horizontalResultTable',
    function () {

      return {
        restrict: 'A',
        scope: {
          resultColumns: '=',
          results: '='
        },
        template: `
<div class="resultsTable">
  <table class="table table-striped">
    <thead>
      <tr>
        <td colspan="2">
          <strong>Results</strong>
        </td>
      </tr>
    </thead>
    <tbody>
      <tr ng-repeat="columnName in resultColumns">
        <td>
          <strong ng-bind="columnName" />
        </td>
        <td>
          <span ng-bind="results[columnName]" />
        </td>
      </tr>
    </tbody>
  </table>
</div>
        `
      };

    }
  );

  directives.directive('verticalResultTable',
    function () {

      return {
        restrict: 'A',
        scope: {
          resultColumns: '=',
          results: '='
        },
        template: `
<div class="resultsTable">
  <table class="table table-striped">
    <thead>
      <tr>
        <td ng-repeat="columnName in resultColumns">
          <strong ng-bind="columnName" />
        </td>
      </tr>
    </thead>
    <tbody>
      <tr ng-repeat="row in results[resultColumns[0]]">
        <td ng-repeat="columnName in resultColumns">
          <span ng-bind="results[columnName][$parent.$index]" />
        </td>
      </tr>
    </tbody>
  </table>
</div>
        `
      };

    }
  );


}());