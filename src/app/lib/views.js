var SharedView, DesktopView, MobileView;


// ================
// SharedView Base Class
// ================
(function () {

	// constructor
	SharedView = function () {
		this.scrollChanged = false;
	};

	SharedView.prototype.onReady = function () {
		this.setDefaults();
	};

	SharedView.prototype.setDefaults = function () {
		//setupDecimalInputEvents();
		//this.setupCarouselEvents();
	};

// end of SharedView class
})();


// ================
// DesktopView Class
// ================
(function () {

	// constructor
	DesktopView = function () {
		this.ensureVisibleTimerId = -1;
		this.hashMonitorIntervalId = -1;
	};

	// base class
	DesktopView.prototype.baseClass = new SharedView();

	DesktopView.prototype.onReady = function () {

		// ensure main and sidebar are visible
		$(".main").show();
		$(".sidebar").show();

		this.ensureVisible();
	};

	DesktopView.prototype.close = function () {
		window.clearInterval(this.ensureVisibleTimerId);
		window.clearInterval(this.hashMonitorIntervalId);
		window.onhashchange = undefined;
	};

	DesktopView.prototype.ensureVisible = function () {
		var maxHeight = $(".nav").height();
		var hdrHeight = $(".header").height();

		var _this = this;
		$(window).scroll(function () {
			_this.baseClass.scrollChanged = true;
		});

		this.ensureVisibleTimerId = window.setInterval(function () {
			if (_this.baseClass.scrollChanged) {
				var activeHeight = $(".carouselSlideActive").height();
				var scrollTop = Math.max($(this).scrollTop() - hdrHeight, 0);

				if ((scrollTop + activeHeight) <= maxHeight)
					$('.carousel').css('top', scrollTop + "px");

				_this.baseClass.scrollChanged = false;
			}

		}, 100);

	};

// end of DesktopView class
})();

// ================
// MobileView Class
// ================
(function () {

	// constructor
	MobileView = function () {

	};

	// base class
	MobileView.prototype.baseClass = new SharedView();

	MobileView.prototype.onReady = function () {

		$("#showMobileMenu").click(onShowMobileMenu);
		$(".nav a").click(onShowMobileMainPanel);

	};

	MobileView.prototype.close = function () {
		$("#showMobileMenu").unbind("click", onShowMobileMenu);
		$(".nav a").unbind("click", onShowMobileMainPanel);

	};

	function onShowMobileMenu(event) {
		$(".main").slideUp("slow", function () {
			$(".sidebar").show();
		});
	}

	function onShowMobileMainPanel(event) {
		$(".sidebar").slideUp("slow", function () {
			$(".main").show();
		});
	}

})();
