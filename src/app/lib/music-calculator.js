var deviceView;

function setupResponsiveDetection() {
  if (matchMedia) {
    var mediaQuery = window.matchMedia("(max-device-width: 768px)");
    mediaQuery.addListener(widthXs);
    widthXs(mediaQuery);

    mediaQuery = window.matchMedia("(min-device-width: 768px)");
    mediaQuery.addListener(widthSd);
    widthSd(mediaQuery);
  }

  function widthXs(mediaQuery) {
    if (mediaQuery.matches)
      widthXsOnReady();
  }

  function widthSd(mediaQuery) {
    if (mediaQuery.matches)
      widthSdOnReady();
  }
}

// onReady for widthXs display size
function widthXsOnReady() {
  if (deviceView != null)
    deviceView.close();

  deviceView = new MobileView();
  deviceView.onReady();
}

// onReady for widthSd
function widthSdOnReady() {
  if (deviceView != null)
    deviceView.close();

  deviceView = new DesktopView();
  deviceView.onReady();
}

$(document).ready(function () {
  setupResponsiveDetection();
});