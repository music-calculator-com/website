(function () {

  "use strict";

  const module = angular.module('app', ['app.controllers', 'app.directives']);

  module.config(['$locationProvider', function ($locationProvider) {
    $locationProvider.hashPrefix('');
  }]);

}());