﻿###
layout: other
title: "404 Error Page"
###
@div class: "copyright-text", ->

	@h1 "Error - Page not found"

	@p ->
		@span "The page you have requested does not exist. It may be that the page url has changed since your last visit."
		@br
		@span "If you wish to go directly to the home page "
		@a href: "/", "click here"
		@span ". Alternatively you can choose from the following quick links:"

	@h2 "Table of contents"

	@dl ->
		@dt ->
			@span class: "ui-helper-reset", ->
				@a href: "/#Bpm", "Bpm Calculations"
		@dd ->
			@a href: "/#Convert_bpm", "Conversion"
		@dd ->
			@a href: "/#Bpm_delay_timing", "Delay timing"
		@dd ->
			@a href: "/#Transpose_bpm_by_percentage", "Transpose by percentage"
		@dd ->
			@a href: "/#Transpose_bpm_by_semitones", "Transpose by semitones"
		@dd ->
			@a href: "/#Generate_bpm_table", "Generate note table"
		@dd " "
		@dt ->
			@span class: "ui-helper-reset", ->
				@a href: "/#Hertz", "Hertz Calculations"
		@dd ->
			@a href: "/#Convert_hertz", "Conversion"
		@dd " "
		@dt ->
			@span class: "ui-helper-reset", ->
				@a href: "/#Note", "Note Calculations"
		@dd ->
			@a href: "/#Convert_note", "Conversion"
		@dd ->
			@a href: "/#Note_difference_in_hertz", "Difference between two notes"
		@dd ->
			@a href: "/#Generate_note_hertz_table", "Generate a hertz table for a specific note"
		@dd ->
			@a href: "/#Generate_octave_hertz_table", "Generate a hertz table for a specific octave"
		@dd " "
		@dt ->
			@span class: "ui-helper-reset", ->
				@a href: "/#Quality_Factor", "Quality Factor Calculations"
		@dd ->
			@a href: "/#Convert_bandwidth_to_quality_factor", "Convert bandwidth to quality factor"
		@dd ->
			@a href: "/#Convert_quality_factor_to_bandwidth", "Convert quality factor to bandwidth"
		@dd " "