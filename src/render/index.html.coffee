###
layout: home
title: "An online tool for producers, sound engineers and musicians"
desc: "An online music calculator that includes formulas and tables for bpm, hertz, keyboard notes and quality factor calculations."
keywords: "calculator,music,bpm,hertz,hz,khz,second,ms,milliseconds,note,key,chart,delay,time,stretch,percentage,semitone,octave,cents,quality,factor,eq,q,filter,LFO,rate,resonance,difference,math,add,subtract,interval,frequency"
###
{partial} = @templateData

@div class: "mobileMenu mvm", ->
	@a id: "showMobileMenu", class:"btn btnDefault", href:"#", "Back to menu"

@div {
	class:"carousel"
	"data-transition-in":"slideInLeft"
	"data-transition-out":"slideOutRight"
	"data-transition-duration": "1"
	"data-interval": "0"}, ->

	@div id: "Bpm", "ng-controller": "bpmController", "ng-init": "init()", ->

		@div class: "carouselSlide carouselSlideActive", ->
			@raw partial("panels/bpm/convert-bpm", false, @templateData)

		@div class: "carouselSlide", ->
			@raw partial("panels/bpm/bpm-delay-timing", false, @templateData)

		@div class: "carouselSlide", ->
			@raw partial("panels/bpm/time-stretch-bpm", false, @templateData)

		@div class: "carouselSlide", ->
			@raw partial("panels/bpm/transpose-bpm-by-percentage", false, @templateData)

		@div class: "carouselSlide", ->
			@raw partial("panels/bpm/transpose-bpm-by-semitones", false, @templateData)

		@div class: "carouselSlide", ->
			@raw partial("panels/bpm/generate-bpm-chart", false, @templateData)

	@div id: "Hertz", "ng-controller": "hertzController", "ng-init": "init()", ->

		@div class: "carouselSlide", ->
			@raw partial("panels/hertz/convert-hertz", false, @templateData)

	@div id: "Note", "ng-controller": "noteController", "ng-init": "init()", ->

		@div class: "carouselSlide", ->
			@raw partial("panels/note/convert-note", false, @templateData)

		@div class: "carouselSlide", ->
			@raw partial("panels/note/note-diff-in-hertz", false, @templateData)

		@div class: "carouselSlide", ->
			@raw partial("panels/note/semitone-difference", false, @templateData)

		@div class: "carouselSlide", ->
			@raw partial("panels/note/note-hertz-table", false, @templateData)

		@div class: "carouselSlide", ->
			@raw partial("panels/note/octave-hertz-table", false, @templateData)

		@div class: "carouselSlide", ->
			@raw partial("panels/note/twelve-tone-table", false, @templateData)

	@div id: "Quality_Factor", "ng-controller": "qualityFactorController", "ng-init": "init()", ->

		@div class: "carouselSlide", ->
			@raw partial("panels/quality-factor/bandwidth-to-quality-factor", false, @templateData)

		@div class: "carouselSlide", ->
			@raw partial("panels/quality-factor/quality-factor-to-bandwidth", false, @templateData)