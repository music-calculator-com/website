###
layout: other
title: "A western twelve tone equal temperament reference table in hertz"
desc: "A twelve tone equal temperament reference table"
keywords: "calculator,music,hertz,hz,khz,note,key,chart,table,q,difference,math,frequency,twelve,tone,equal,temperament"
###
@div ->
	@h2 "A western twelve tone equal temperament reference table in hertz. C4 is the middle C"

@div class:"tam", ->
	@table class: "table", ->
		@thead ->
			@tr ->
				@td " "
				@td "0"
				@td "1"
				@td "2"
				@td "3"
				@td "4"
				@td "5"
				@td "6"
				@td "7"
				@td "8"
				@td "9"
				@td "10"

		@tbody ->
			@tr ->
				@td "C"
				@td "16.352"
				@td "32.703"
				@td "65.406"
				@td "130.813"
				@td "261.626"
				@td "523.251"
				@td "1046.502"
				@td "2093.005"
				@td "4186.009"
				@td "8372.018"
				@td "16744.036"

			@tr class: "tableRowAlt", ->
				@td "C#/Db"
				@td "17.324"
				@td "34.648"
				@td "69.296"
				@td "138.591"
				@td "277.183"
				@td "554.365"
				@td "1108.731"
				@td "2217.461"
				@td "4434.922"
				@td "8869.844"
				@td "17739.688"

			@tr ->
				@td "D"
				@td "18.354"
				@td "36.708"
				@td "73.416"
				@td "146.832"
				@td "293.665"
				@td "587.33"
				@td "1174.659"
				@td "2349.318"
				@td "4698.636"
				@td "9397.273"
				@td "18794.545"

			@tr class: "tableRowAlt", ->
				@td "D#/Eb"
				@td "19.445"
				@td "38.891"
				@td "77.782"
				@td "155.563"
				@td "311.127"
				@td "622.254"
				@td "1244.508"
				@td "2489.016"
				@td "4978.032"
				@td "9956.063"
				@td "19912.127"

			@tr ->
				@td "E"
				@td "20.602"
				@td "41.203"
				@td "82.407"
				@td "164.814"
				@td "329.628"
				@td "659.255"
				@td "1318.51"
				@td "2637.02"
				@td "5274.041"
				@td "10548.082"
				@td "21096.164"

			@tr class: "tableRowAlt", ->
				@td "F"
				@td "21.827"
				@td "43.654"
				@td "87.307"
				@td "174.614"
				@td "349.228"
				@td "698.456"
				@td "1396.913"
				@td "2793.826"
				@td "5587.652"
				@td "11175.303"
				@td "22350.607"

			@tr ->
				@td "F#/Gb"
				@td "23.125"
				@td "46.249"
				@td "92.499"
				@td "184.997"
				@td "369.994"
				@td "739.989"
				@td "1479.978"
				@td "2959.955"
				@td "5919.911"
				@td "11839.822"
				@td "23679.643"

			@tr class: "tableRowAlt", ->
				@td "G"
				@td "24.5"
				@td "48.999"
				@td "97.999"
				@td "195.998"
				@td "391.995"
				@td "783.991"
				@td "1567.982"
				@td "3135.963"
				@td "6271.927"
				@td "12543.854"
				@td "25087.708"

			@tr ->
				@td "G#/Ab"
				@td "25.957"
				@td "51.913"
				@td "103.826"
				@td "207.652"
				@td "415.305"
				@td "830.609"
				@td "1661.219"
				@td "3322.438"
				@td "6644.875"
				@td "13289.75"
				@td "26579.501"

			@tr class: "tableRowAlt", ->
				@td "A"
				@td "27.5"
				@td "55"
				@td "110"
				@td "220"
				@td "440"
				@td "880"
				@td "1760"
				@td "3520"
				@td "7040"
				@td "14080"
				@td "28160"

			@tr ->
				@td "A#/Bb"
				@td "29.135"
				@td "58.27"
				@td "116.541"
				@td "233.082"
				@td "466.164"
				@td "932.328"
				@td "1864.655"
				@td "3729.31"
				@td "7458.62"
				@td "14917.24"
				@td "29834.481"

			@tr class: "tableRowAlt", ->
				@td "B"
				@td "30.868"
				@td "61.735"
				@td "123.471"
				@td "246.942"
				@td "493.883"
				@td "987.767"
				@td "1975.533"
				@td "3951.066"
				@td "7902.133"
				@td "15804.266"
				@td "31608.531"
