@section id: "convert-hertz", class: "panel panel-calc", ->

	@div class: "panel-heading", ->

		@h1 class: "h1 mvm", ->
			@span "Hertz Conversion"

	@div class: "panel-body txtC", ->

		@div class: "mvm", ->

			@div ->
				@label for: "ConvertHertzA", "Enter the hertz value you wish to convert"

			@div ->
				@input class: "numberInput", id: "ConvertHertzA", type: "number", "ng-model": "convertHertzA", "ui-spinner": true, "ui-spinner-change": "onConvertHertzChange()"

		@div "horizontal-result-table": "true", "result-columns": "convertHertzResultsColumns", "results": "convertHertzResults"