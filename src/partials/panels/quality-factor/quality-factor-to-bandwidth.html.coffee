@section id: "convert-quality-factor-to-bandwidth", class: "panel panel-calc", ->
	@div class: "panel-heading", ->
		@h1 class: "h1 mvm", ->
			@span "Convert quality factor to bandwidth"

	@div class: "panel-body txtC", ->
		@div class: "mvm", ->
			@label "Enter the quality factor you wish to convert"

			@div ->
				@input class: "decimal3Input", id: "ConvertQtoBWA", type: "number", "ng-model": "convertQtoBWA", "ui-spinner": true, "ui-spinner-change": "onConvertQtoBWChange()"

			@div "horizontal-result-table": "true", "result-columns": "convertQtoBWResultsColumns", "results": "convertQtoBWResults"