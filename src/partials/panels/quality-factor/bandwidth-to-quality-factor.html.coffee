@section id: "convert-bandwidth-to-quality-factor", class: "panel panel-calc", ->
	@div class: "panel-heading", ->
		@h1 class: "h1 mvm", ->
			@span "Convert bandwidth to quality factor"

	@div class: "panel-body txtC", ->
		@div class: "mvm", ->
			@div ->
				@label for:"ConvertBWtoQA", "Enter the octave part of the bandwidth"
			@div ->
				@select id: "ConvertBWtoQA",
							"ng-model": "convertBWtoQA",
							"ng-options": "value as value for value in octaves",
							"ng-change": "onConvertBWtoQChange()"

			@div ->
				@label for:"ConvertBWtoQB", "Enter the semitone part of the bandwidth"
			@div ->
				@select id: "ConvertBWtoQB",
						"ng-model": "convertBWtoQB",
						"ng-options": "value as value for value in semitones",
						"ng-change": "onConvertBWtoQChange()"

			@div "horizontal-result-table": "true", "result-columns": "convertBWtoQResultsColumns", "results": "convertBWtoQResults"