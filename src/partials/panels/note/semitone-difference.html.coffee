@section id: "semitone-difference", class: "panel panel-calc", ->
	@div class: "panel-heading", ->
		@h1 class: "h1 mvm", ->
			@span "Note Semitone difference"

	@div class: "panel-body txtC", ->

		@div class: "mvm", ->
			@div ->
				@label for: "SemitoneDifferenceA", "Select that starting note and octave"
			@div ->
				@select class: "txtM", id: "SemitoneDifferenceA", "ng-model": "semitoneDifferenceA", "ng-change": "onSemitoneDifferenceChange()", ->
					@option "ng-repeat": "name in noteNames", value: "{{$index}}", "ng-bind": "name"

				@raw "&nbsp;"
				@select class: "txtM", id: "SemitoneDifferenceB",
						"ng-model": "semitoneDifferenceB",
						"ng-change": "onSemitoneDifferenceChange()",
						"ng-options": "octave for octave in octaves"

			@div ->
				@label for: "SemitoneDifferenceC", "Enter a semitone value"

			@div ->
				@input class: "decimalInput", id: "SemitoneDifferenceC", "ng-model": "semitoneDifferenceC", "ui-spinner": true, "ui-spinner-change": "onSemitoneDifferenceChange()", ->
					@span " semitones"

			@div "horizontal-result-table": "true", "result-columns": "semitoneDifferenceResultsColumns", "results": "semitoneDifferenceResults"