@section id: "twelve-tone-equal-temperament-table-in-hertz", class: "panel panel-calc", ->

	@div class: "panel-heading", ->
		@h1 class: "h1 mvm", ->
			@span "Twelve tone equal temperament table in hertz"

	@div class: "panel-body txtC", ->
		@div class: "mvm", ->
			@span "This is a pre-genereated table which shows all octaves and notes in hertz."

		@div class: "mvm", ->
			@a class: "btn btnDefault", href: "twelve-tone-equal-temperament-table.html", "Click here to view"

		@div id: "GenerateFullNoteHertzTableResult", class:"resultsTable"
