@section id: "convert-note", class: "panel panel-calc", ->

	@div class: "panel-heading", ->
		@h1 class: "h1 mvm", ->
			@span "Conversion"

	@div class: "panel-body txtC", ->

		@div class: "mvm", ->

			@div ->
				@label for:"ConvertNoteA", "Select the note and octave you wish to convert"

			@div ->
				@select class: "txtM", id: "ConvertNoteA", "ng-model": "convertNoteA", "ng-change": "onConvertNoteChange()", ->
					@option "ng-repeat": "name in noteNames", value: "{{$index}}", "ng-bind": "name"

				@raw "&nbsp;"
				@select class: "txtM", id: "ConvertNoteB",
						"ng-model": "convertNoteB",
						"ng-options": "octave for octave in octaves",
						"ng-change": "onConvertNoteChange()"

			@div "horizontal-result-table": "true", "result-columns": "convertNoteResultsColumns", "results": "convertNoteResults"