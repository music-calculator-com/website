@section id: "generate-octave-hertz-table", class: "panel panel-calc", ->
	@div class: "panel-heading", ->
		@h1 class: "h1 mvm", ->
			@span "Generate a hertz table for a specific octave"

	@div class: "panel-body txtC", ->
		@div class: "mvm", ->
			@div ->
				@label for: "GenerateNoteOctaveHertzTableA", "Choose an octave "
			@div ->
				@select id: "GenerateNoteOctaveHertzTableA",
					"ng-model": "generateNoteOctaveHertzTableA",
					"ng-options": "octave for octave in octaves",
					"ng-change": "onGenerateNoteOctaveHertzTable()"

		@div id: "GenerateNoteOctaveHertzTableResult", class:"resultsTable"

		@div "vertical-result-table": "true", "result-columns": "generateNoteOctaveHertzTableResultsColumns", "results": "generateNoteOctaveHertzTableResults"
