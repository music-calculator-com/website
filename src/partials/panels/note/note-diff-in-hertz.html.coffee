@section id:"note-difference-in-hertz", class: "panel panel-calc", ->

	@div class: "panel-heading", ->
		@h1 class: "h1 mvm", ->
			@span "Difference between two notes"

	@div class: "panel-body txtC", ->
		@div class: "mvm", ->
		
			@div ->
				@label for:"NoteDiffInHertzA", "Select the first note and octave"
			@div ->
				@select class: "txtM", id: "NoteDiffInHertzA", "ng-model": "noteDiffInHertzA", "ng-change": "onNoteDiffInHertzChange()", ->
					@option "ng-repeat": "name in noteNames", value: "{{$index}}", "ng-bind": "name"

				@raw "&nbsp;"
				@select class: "txtM", id: "NoteDiffInHertzB",
						"ng-model": "noteDiffInHertzB",
						"ng-options": "octave for octave in octaves",
						"ng-change": "onNoteDiffInHertzChange()"

			@div ->
				@label for:"NoteDiffInHertzC", "Select the second note and octave"
			@div ->
				@select class: "txtM", id: "NoteDiffInHertzC", "ng-model": "noteDiffInHertzC", "ng-change": "onNoteDiffInHertzChange()", ->
					@option "ng-repeat": "name in noteNames", value: "{{$index}}", "ng-bind": "name"

				@raw "&nbsp;"
				@select class: "txtM", id: "NoteDiffInHertzD",
						"ng-model": "noteDiffInHertzD",
						"ng-options": "octave for octave in octaves",
						"ng-change": "onNoteDiffInHertzChange()"

			@div "horizontal-result-table": "true", "result-columns": "noteDiffInHertzResultsColumns", "results": "noteDiffInHertzResults"