@section id: "generate-note-hertz-table", class: "panel panel-calc", ->
	@div class: "panel-heading", ->
		@h1 class: "h1 mvm", ->
			@span "Generate a hertz table for a specific note"

	@div class: "panel-body txtC", ->
		@div class: "mvm", ->
			@div ->
				@label for: "GenerateNoteHertzTableA", "Choose a note "
			@div ->
				@select id: "GenerateNoteHertzTableA", "ng-model": "generateNoteHertzTableA", "ng-change": "onGenerateNoteHertzTableChange()", ->
					@option "ng-repeat": "name in noteNames", value: "{{$index}}", "ng-bind": "name"

		@div "vertical-result-table": "true", "result-columns": "generateNoteHertzTableResultsColumns", "results": "generateNoteHertzTableResults"