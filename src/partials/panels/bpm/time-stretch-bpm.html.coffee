@section id: "time-stretch-bpm", class: "panel panel-calc", ->

	@div class: "panel-heading", ->
		@h1 class: "h1 mvm", ->
			@span "Bpm time stretch"

	@div class: "panel-body", ->
		@div class: "mvm", ->
			@div ->
				@label "Enter the original bpm"
			@div ->
				@input class: "decimalInput", id: "TimeStretchBpmA", type: "number", "ng-model": "bpmTimeStretchA", "ui-spinner": true, "ui-spinner-change": "onBpmTimeStretchChange()"
			@div ->
				@label "Enter the bpm to stretch to"
			@div ->
				@input class: "decimalInput", id: "TimeStretchBpmB", type: "number", "ng-model": "bpmTimeStretchB", "ui-spinner": true, "ui-spinner-change": "onBpmTimeStretchChange()"

		@div "horizontal-result-table": "true", "result-columns": "bpmTimeStretchResultsColumns", "results": "bpmTimeStretchResults"