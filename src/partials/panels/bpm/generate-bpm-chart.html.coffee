@section id:"generate-bpm-table", class: "panel panel-calc", ->

	@div class: "panel-heading", ->
		@h1 class: "h1 mvm", ->
			@span "Generate Bpm note chart"

	@div class: "panel-body", ->
		@div class: "mvm", ->
			@div ->
				@label for:"GenerateBpmChartA", "Enter the minimum bpm"
			@div ->
				@input class: "decimalInput", id: "GenerateBpmChartA", type: "number", "ng-model": "generateBpmChartA", "ui-spinner": true, "ui-spinner-change": "onGenerateNoteBpmTableChange()"

			@div ->
				@label for:"GenerateBpmChartB", "Enter the maximum bpm"
			@div ->
				@input class: "decimalInput", id: "GenerateBpmChartB", type: "number", "ng-model": "generateBpmChartB", "ui-spinner": true, "ui-spinner-change": "onGenerateNoteBpmTableChange()"

			@div ->
				@label "for": "GenerateBpmChartC",
						"Enter the amount of bpm to step between the minimum and maximum bpm values"

			@div ->
				@input class: "decimalInput", id: "GenerateBpmChartC", type: "number", "ng-model": "generateBpmChartC", "ui-spinner": true, "ui-spinner-change": "onGenerateNoteBpmTableChange()"

		@div "vertical-result-table": "true", "result-columns": "generateNoteBpmTableResultsColumns", "results": "generateNoteBpmTableResults"