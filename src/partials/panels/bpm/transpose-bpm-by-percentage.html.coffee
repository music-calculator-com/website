@section id:"transpose-bpm-by-percentage", class: "panel panel-calc", ->

	@div class: "panel-heading", ->
		@h1 class: "h1 mvm", ->
			@span "Transpose Bpm by percentage"

	@div class: "panel-body", ->
		@div class: "mvm", ->
			@div ->
				@label for:"TransposeBpmByPercentageA", "Enter the original bpm "
			@div ->
				@input class: "decimalInput", id: "TransposeBpmByPercentageA", type: "number", "ng-model": "transposeBpmByPercentageA", "ui-spinner": true, "ui-spinner-change": "onBpmTransposeByPercentageChange()"

			@div ->
				@label for:"TransposeBpmByPercentageB", "Enter the percentage to transpose by"
			@div ->
				@input class: "numberInput", id: "TransposeBpmByPercentageB", type: "number", "ng-model": "transposeBpmByPercentageB", "ui-spinner": true, "ui-spinner-change": "onBpmTransposeByPercentageChange()"


		@div "horizontal-result-table": "true", "result-columns": "bpmTransposeByPercentageResultsColumns", "results": "bpmTransposeByPercentageResults"