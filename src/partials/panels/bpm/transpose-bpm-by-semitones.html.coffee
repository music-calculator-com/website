@section id: "transpose-bpm-by-semitones", class: "panel panel-calc", ->

	@div class: "panel-heading", ->
		@h1 class: "h1 mvm", ->
			@span "Transpose Bpm by semitones"

	@div class: "panel-body", ->

		@div class: "mvm", ->

			@div ->
				@label for: "TransposeBySemitonesA", "Enter the original bpm"
			@div ->
				@input class: "decimalInput", id: "TransposeBySemitonesA", type: "number", "ng-model": "transposeBpmBySemitonesA", "ui-spinner": true, "ui-spinner-change": "onBpmTransposeBySemitonesChange()"

			@div ->
				@label for:"TransposeBySemitonesB", "Enter the semitones to transpose by"
			@div ->
				@input class: "numberInput", id: "TransposeBySemitonesB", type: "number", "ng-model": "transposeBpmBySemitonesB", "ui-spinner": true, "ui-spinner-change": "onBpmTransposeBySemitonesChange()"

		@div "horizontal-result-table": "true", "result-columns": "bpmTransposeBySemitonesResultsColumns", "results": "bpmTransposeBySemitonesResults"

#		@div id: "TransposeBySemitonesResult", class:"resultsTable", ->
#
#			@div ->
#				@div class: "table-header table-header-end", ->
#					@strong "Results"
#
#				@div style: "clear: both;", ->
#					@div class: "table-row-top table-row", ->
#						@span "bpm"
#					@div class: "table-row-top table-row table-row-end", " "
#
#				@div style: "clear: both;", ->
#					@div class: "table-row", "% change"
#					@div class: "table-row table-row-end", " "
#
#				@div style: "clear: both;", ->
#					@div class: "table-row", ->
#						@span "note"
#					@div class: "table-row table-row-end", " "
#
#				@div style: "clear: both;", ->
#					@div class: "table-row", ->
#						@span "octave"
#					@div class: "table-row table-row-end", " "
#
#				@div style: "clear: both;", ->
#					@div class: "table-row", ->
#						@span "cents"
#					@div class: "table-row table-row-end", " "
