@section id: "convert-bpm", class: "panel panel-calc", ->

	@div class: "panel-heading", ->
		@h1 class: "h1 mvm", ->
			@span "Bpm Conversion"

	@div class: "panel-body", ->
		@div class: "mvm", ->
			@label for:"ConvertBpmA", "Enter the bpm value you wish to convert"

		@div ->
			@input class: "decimalInput", id: "ConvertBpmA", type: "number", "ng-model": "convertBpmValue", "ui-spinner": true, "ui-spinner-change": "onConvertBpmChange()"


		@div "horizontal-result-table": "true", "result-columns": "convertBpmResultsColumns", "results": "convertBpmResults"