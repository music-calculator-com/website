@section id: "bpm-delay-timing", class: "panel panel-calc", ->

	@div class: "panel-heading", ->
		@h1 class: "h1 mvm", ->
			@span "Bpm Delay timing"

	@div class: "panel-body", ->
		@div class: "mvm", ->
			@div ->
				@label for:"BpmDelayTimingA", "Enter the bpm value"
			@div ->
				@input class: "decimalInput", id: "BpmDelayTimingA", "ng-model": "bpmDelayTimingA", "ui-spinner": true, "ui-spinner-change": "onBpmDelayTimingChange()"
			@div ->
				@label for:"BpmDelayTimingB", "Enter the delay division value"
			@div ->
				@text "1/"
				@input class: "numberInput", id: "BpmDelayTimingB", type: "number", "ng-model": "bpmDelayTimingB", "ui-spinner": true, "ui-spinner-change": "onBpmDelayTimingChange()"
				@raw "&nbsp;"
				@select class: "txtM", id: "BpmDelayTimingC",
						"ng-model": "bpmDelayTimingC",
						"ng-options": "value as value for value in noteTypes",
						"ng-change": "onBpmDelayTimingChange()"


	    @div "horizontal-result-table": "true", "result-columns": "bpmDelayTimingResultsColumns", "results": "bpmDelayTimingResults"