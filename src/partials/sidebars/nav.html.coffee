@aside role: "navigation", ->

	@div class: "nav", ->

		@ul class: "listBordered", ->

			@li ->
				@span class: "h5", "Bpm Calculations"

				@ul ->
					@li ->
						@a class: "btn navLinkActive", href: "#convert-bpm", "Conversion"
					@li ->
						@a class: "btn", href: "#bpm-delay-timing", "Delay timing"
					@li ->
						@a class: "btn", href: "#time-stretch-bpm", "Time stretch"
					@li ->
						@a class: "btn", href: "#transpose-bpm-by-percentage", "Transpose by percentage"
					@li ->
						@a class: "btn", href: "#transpose-bpm-by-semitones", "Transpose by semitones"
					@li ->
						@a class: "btn", href: "#generate-bpm-table", "Generate bpm chart"

			@li ->
				@span class: "h5",  "Hertz Calculations"

				@ul ->
					@li ->
						@a class: "btn", href: "#convert-hertz", "Conversion"

			@li ->
				@span class: "h5",  "Note Calculations"

				@ul ->
					@li ->
						@a class: "btn", href: "#convert-note", "Conversion"
					@li ->
						@a class: "btn", href: "#note-difference-in-hertz", "Note difference"
					@li ->
						@a class: "btn", href: "#semitone-difference", "Semitone difference"
					@li ->
						@a class: "btn", href: "#generate-note-hertz-table", "Note hertz table generator"
					@li ->
						@a class: "btn", href: "#generate-octave-hertz-table", "Octave hertz table generator"
					@li ->
						@a class: "btn", href: "#twelve-tone-equal-temperament-table-in-hertz", "Twelve tone hertz table"

			@li ->
				@span class: "h5",  "Quality Factor Calculations"

				@ul ->
					@li ->
						@a class: "btn", href: "#convert-bandwidth-to-quality-factor", "Bandwidth to Q"
					@li ->
						@a class: "btn", href: "#convert-quality-factor-to-bandwidth", "Q to bandwidth"