{site} = @templateData

@script type: "application/javascript", src: "assets/js/vendor-bundle.min.js?#{site.version}"
@script type: "application/javascript", src: "assets/jquery-ui/jquery-ui-1.10.3.custom.min.js?#{site.version}"
@script type: "application/javascript", src: "assets/js/app-bundle.min.js?#{site.version}"

# google ads
@script src:"//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js", async: "true"
@script """(adsbygoogle = window.adsbygoogle || []).push({});"""


