{site} = @templateData

# determine keywords
if @document.tags
	tags = @document.tags.join(" ")

keywords = (@document.keywords || "") + " " + (tags || "") + " " + (site.keywords || "")

# determine canonical
if site.url[site.url.length - 1] == '/' and @document.url[0] == '/'
	canonical = site.url + @document.url.slice(1)
else
	canonical = site.url + @document.url

canonical = canonical.replace("index.html", "")

# begin html tag output
@title @document.title || @document.filename

# write the meta
@meta charset: 'utf-8'

@meta name: "viewport", content: "width=device-width, initial-scale=1.0"
@meta name: "description", content: @document.description || @document.desc || ""
@meta name: "keywords", content: keywords.trim()
@meta name: "copyright", content: site.copyright
@meta name: "google-site-verification", content: "t2Bs2ycV4caaC9m-01vpXCIgYP69Hb6uUSceVfyTAag"

@meta "http-equiv": "X-UA-Compatible", content: "IE=edge,chrome=1"
@meta "http-equiv": "content-language", content: "en-gb"

# write the links
@link rel: "canonical", href: canonical

# vendor css
@link rel: "stylesheet", media: "all", href: "assets/css/vendor-bundle.min.css?#{site.version}", type: "text/css"


# app css
@link rel: "stylesheet", media: "all", href: "assets/css/app-bundle.min.css?#{site.version}", type: "text/css"

# jquery ui
@link rel: "stylesheet", href: "assets/jquery-ui/redmond/jquery-ui-1.10.3.custom.min.css?#{site.version}", type: "text/css"

@link rel: "icon", href: "favicon.ico", type: "image/x-icon"
@link rel: "shortcut icon", href: "favicon.ico", type: "image/x-icon"

# js detection class replace code
@script """(function (H) {
	H.className = H.className.replace(/\\bnoJS\\b/g, 'js')
})(document.documentElement)"""
