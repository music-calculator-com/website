{site} = @templateData

# header
@header class: "header", role: "banner", ->
	@div class:"line", ->
		@div class:"col cols4of12", ->
			@a href: "#{site.baseUrl}", ->
				@span class:"logoImg"

		@div class:"col lastCol txtR", ->
			@div class:"headerAdv", ->
				@ins
					class: "adsbygoogle"
					style: "display:inline-block;width:468px;height:60px"
					"data-ad-client": "ca-pub-8769601628010061"
					"data-ad-slot": "5106288201"
