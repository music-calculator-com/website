{docpad} = @context
{site} = @templateData

@footer class:"footer mal", role:"contentinfo", ->
	@div class:"container", ->
		@aside class:"aside", role:"complementary", ->
			@ul class:"listInline txtR", ->
				if docpad.config.env == "development" or !docpad.config.env
					@li ->
						@a href: "#{site.baseUrl}oocss/help/components.html", ->
							@text "This Website Component Library"