{getBlock, partial} = @templateData

@doctype 5

@html class:"noJS", lang: "en", "ng-app": "app", ->

	@head ->
		@raw partial("shared/ga.html")
		@raw partial("shared/head.html.coffee", false, @templateData)

	@body {"ng-controller":"appController", "ng-init": "init()"}, ->

		@div class: "container container-app", ->

			# header
			@div class: "row", ->
				@raw partial("shared/header", false, @templateData)

			@div class: "row", ->

				@div class: "col-sm-3 sidebar", ->
					# left nav sidebar
					@raw partial("sidebars/nav", false, @templateData)

				@div class: "col-sm-9 main", role: "main", ->
					# main
					@raw @content

		# scripts after
		@raw partial("shared/scripts_after", false, @templateData)

		@raw getBlock("scripts").toHTML()