{site, getBlock, partial} = @templateData

try
	@doctype 5
	@html class:"noJS", lang: "en", ->

		@head ->
			@raw partial("shared/head.html.coffee", false, @templateData)

		@body ->

			@div  class: "content home", ->
				@div  class: "container", ->
					@div class: "contentInner", ->

						# header
						@raw partial("shared/header", false, @templateData)

						# main
						@div class:"main", role: "main", id: "main", ->
							@raw @content

			# scripts after
			@raw partial("shared/scripts_after", false, @templateData)

			@raw getBlock("scripts").toHTML()
catch err
	console.log err
