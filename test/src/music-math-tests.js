(function () {

	'use strict';

	describe('musicMathService', function () {

		var musicMathService;

		// load the module and mocks
		beforeEach(function () {

			module('app');

			inject(function (_musicMathService_) {
				musicMathService = _musicMathService_;
			});

		});



		// ================
		// Semitone tests
		// ================
		it("Can Semitone shift a semitone", function () {
			// setup
			var valueToShift = 120,
				amountToShift = 12;
			var expected = 240;

			// test
			var actual = musicMathService.semitoneShift(valueToShift, amountToShift);

			// assert
			expect(actual).toBe(expected);

		});

		it("Can find the difference between two semitones", function () {
			// setup
			var value1 = 120,
				value2 = 240;
			var expected = 12;

			// test
			var actual = musicMathService.semitoneDiff(value1, value2);

			// assert
			expect(actual).toBe(expected);

		});

		// ================
		// BPM tests
		// ================
		it("Can Convert BPM to a Midi Note", function () {
			// setup
			var bpm = 120;
			var expected = {
				octave: -4,
				noteName: "C",
				cents: -37.631656229592636
			};

			// test
			var actual = musicMathService.bpmToMidiNote(bpm);

			// assert
			expect(actual.octave).toBe(expected.octave);
			expect(actual.noteName).toBe(expected.noteName);
			expect(actual.cents).toBe(expected.cents);

		});

		it("Can Convert BPM to Hertz", function () {
			// setup
			var bpm = 120;
			var expected = 2;

			// test
			var actual = musicMathService.bpmToHz(bpm);

			// assert
			expect(actual).toBe(expected);

		});

		// ================
		// Hz tests
		// ================
		it("Can Convert a Musical Note to Hertz", function () {
			// setup
			var octave = 4, note = 9;
			var expected = 440;

			// test
			var actual = musicMathService.noteToHz(note - 9, octave - 4);

			// assert
			expect(actual).toBe(expected);

		});

		it("Can Convert Hertz to BPM", function () {
			// setup
			var hertz = 2;
			var expected = 120;

			// test
			var actual = musicMathService.hzToBpm(hertz);

			// assert
			expect(actual).toBe(expected);

		});

		// ================
		// Quality factor tests
		// ================
		it("Can Convert Bandwidth to Quality Factor", function () {
			// setup
			var octave = 3, semitone = 6;
			var delta = octave + (semitone * musicMathService.semitoneQBWConst);
			var expected = 0.32612766410316485;

			// test
			var actual = musicMathService.octaveBWtoQ(delta);

			// assert
			expect(actual).toBe(expected);

		});

		// ================
		it("Can Convert Quality Factor to Bandwidth", function () {
			// setup
			var qualityFactor = 0.32612766410316485;
			var expected = 3.5;

			// test
			var actual = musicMathService.qToOctaveBW(qualityFactor);

			// assert
			expect(actual).toBe(expected);

		});

	});

}());
