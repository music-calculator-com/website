module.exports = function (config) {

  "use strict";

  return config.set({
    basePath: '../',
    port: 9876,
    frameworks: ['jasmine'],
    reporters: ['dots', 'coverage'],
    log: 'karma/karma.log',
    colors: true,
    plugins: [
      'karma-chrome-launcher',
      'karma-edge-launcher',
      'karma-firefox-launcher',
      'karma-jasmine',
      'karma-coverage'
    ],
    preprocessors: {
      'src/**/*.js': ['coverage']
    },
    coverageReporter: {
      type: 'html',
      dir: 'test/karma/coverage/'
    },
    files: [
      'out/assets/js/vendor-bundle.min.js',
      'node_modules/angular-mocks/angular-mocks.js',
      'out/assets/js/app-bundle.min.js',
      // tests
      'test/src/music-math-tests.js'
    ],
    browsers: ['Chrome'],
    singleRun: false
  });

};
