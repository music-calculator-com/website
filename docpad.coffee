# DocPad Configuration File
# http://docpad.org/docs/config

# Define the DocPad Configuration
docpadConfig = {
	
	databaseCache: false
	
	# Reload Paths
	# An array of special paths that when changes occur in, we reload our configuration
	reloadPaths: [
		'./config'
	]

	# =================================
	# Template Configuration

	# Template Data
	# Use to define your own template data and helpers that will be accessible to your templates
	# Complete listing of default values can be found here: http://docpad.org/docs/template-data
	templateData: require("./config/templateData")

	# =================================
	# Main Plugin Configuration
	# plugins: require("./config/plugins")

	# =================================
	# Collections
	# A hash of functions that create collections
	#collections: require("./config/collections")

	# =================================
	# Event Configuration

	# Events
	# Allows us to bind listeners to the events that DocPad emits
	# Complete event listing can be found here: http://docpad.org/docs/events
	#events: require("./config/events")


	# =================================
	# Server Configuration
	
	# Port
	# Use to change the port that DocPad listens to
	# By default we will detect the appropriate port number for our environment
	# if no environment port number is detected we will use 9778 as the port number
	port: null  # uses 9778 as default


	# =================================
	# Paths Configuration
	
	# Out Path
	# Where should we put our generated website files?
	# If it is a relative path, it will have the resolved `rootPath` prepended to it
	outPath: 'out'  # default
	
	
	# =================================
	# Other Configuration

	# Prompts
	# Whether or not we should display any prompts (used to suppress the tos agreement in build scripts)
	prompts: false


	# =================================
	# Environments Configuration

	# Environments
	# Allows us to override and add custom configuration for specific environments
	environments:
		development:
			templateData: require("./config/templateData.dev")
			#plugins: require("./config/plugins.dev")

}


# clear the require cache so we can live reload "./config" files
dir = require('path').resolve(__dirname, "./config")

require('fs').readdir dir, (err, files) ->
	files.forEach (filename) ->
		delete require.cache[require('path').join(dir, filename)]

# Export the DocPad Configuration
module.exports = docpadConfig