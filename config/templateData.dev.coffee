﻿module.exports =
    # Template Data
    # Use to define your own template data and helpers that will be accessible to your templates
    # Complete listing of default values can be found here: http://docpad.org/docs/template-data
    site:
        baseUrl: ''
        url: "//localhost:9778/"
        mainHeading: "DEV: Music Calculator"
        services:
            googleAnalytics: 'UA-10651889-2'
            disqus: "musiccalculator-test"