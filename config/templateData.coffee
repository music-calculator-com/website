﻿module.exports =
	# Template Data
	# Use to define your own template data and helpers that will be accessible to your templates
	# Complete listing of default values can be found here: http://docpad.org/docs/template-data
	site:
		keywords: ""
		version: "1.4.0"
		baseUrl: '/'
		url: "//musiccalculator.com/"
		mainHeading: "Music Calculator"
		copyright: "Copyright (c) 2013 Stringz Solutions Ltd. All Rights Reserved."

		services:
			googleAnalytics: "UA-44940223-1"
			disqus: "musiccalculator"

			githubFollowButton: "musiccalculator"
			twitterFollowButton: 'musiccalculator'
			gittipButton: 'musiccalculator'

			twitterTweetButton: 'musiccalculator'

	logError: (err, source) ->
		docpad.log "error", source
		docpad.log "error", err.message
		docpad.log "error", err.stack