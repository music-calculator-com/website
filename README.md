# MusicCalculator.com

[![build status](https://gitlab.com/music-calculator-com/website/badges/master/build.svg)](https://gitlab.com/music-calculator-com/website/commits/master)

## License
Licensed under the incredibly [permissive](http://en.wikipedia.org/wiki/Permissive_free_software_licence) [MIT License](http://creativecommons.org/licenses/MIT/)
<br/>Copyright &copy; 2013+ Stringz Solutions Ltd
<br/>Copyright &copy; 2013+ [Peter Flannery](http://gitlab.com/pflannery)
